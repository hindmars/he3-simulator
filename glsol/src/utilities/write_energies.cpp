#define USE_MPI 
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
//#include <math.h>
#include <assert.h>

#include "plumbing/hila.h"
#include "plumbing/fft.h"

#include "glsol.hpp"
#include "matep.hpp"


void glsol::write_energies() {

  Matrix<3,3,Complex<double>> sumA;
  sumA = 0.0;

  Complex<double> sumgapA{0};
  Complex<double> sumAgap{0};
  Complex<double> suma(0),sumb2(0),sumb3(0),sumb4(0),sumb5(0);
  Complex<double> suma_we(0),sumb2_we(0),sumb3_we(0),sumb4_we(0),sumb5_we(0);
  Complex<double> sumk1(0),sumk2(0),sumk3(0);
  Complex<double> sumk1_we(0),sumk2_we(0),sumk3_we(0);
  real_t sumb1 = 0;
  Complex<double> sumb1_we = 0;
  Complex<double> sumkin(0);
  Complex<double> sumkin_we(0);

  hila::set_allreduce(false);
  onsites(ALL) {

    Complex<double> gapA {0};
    Complex<double> a(0),b2(0),b3(0),b4(0),b5(0);
    Complex<double> kin(0);
    Complex<double> k1(0), k2(0), k3(0);
    Complex<double> bfe(0);
    double b1 = 0;

    real_t ebfe=fmin(MP.f_A_td(config.Inip, T[X]),MP.f_B_td(config.Inip, T[X])); 

    //real_t ebfe=fmin(MP.f_A_td(20.0, 2.2),MP.f_B_td(20.0, 2.2));
     
    //real_t ebfe = 1.0;
      
    real_t beta[6];
    point_params(T[X], config.Inip, beta);

    gapA = sqrt((A[X]*(A[X].dagger())).trace());
    
    a = beta[0] * (A[X]*A[X].dagger()).trace();

    b1 = beta[1] * ((A[X]*A[X].transpose()).trace()).squarenorm();

    b2 = beta[2] * ((A[X]*A[X].dagger()).trace()*(A[X]*A[X].dagger()).trace());

    b3 = beta[3] * ((A[X]*A[X].transpose()*A[X].conj()*A[X].dagger()).trace());

    b4 = beta[4] * ((A[X]*A[X].dagger()*A[X]*A[X].dagger()).trace());

    b5 = beta[5] * ((A[X]*A[X].dagger()*A[X].conj()*A[X].transpose()).trace());

    bfe = a + b1 + b2 + b3 + b4 + b5 - ebfe;

            
    kin = (pi[X]*pi[X].dagger()).trace();
      
    foralldir(j) foralldir (k) foralldir(al){
      k1 += squarenorm(A[X + k].e(al,j) - A[X - k].e(al,j)) / (4.0*sqr(config.dx));
      k2 += (A[X + j].e(al,j) - A[X - j].e(al,j)) * (A[X + k].e(al,k) - A[X - k].e(al,k)).conj() / (4.0*sqr(config.dx));
      k3 += (A[X + k].e(al,j) - A[X - k].e(al,j)) * (A[X + j].e(al,k) - A[X - j].e(al,k)).conj() / (4.0*sqr(config.dx));
      
	// k1 += (A[X + k].column(j) - A[X - k].column(j)).e(al)
	//       * (A[X + k].conj().column(j) - A[X - k].conj().column(j)).e(al)/(4.0*config.dx*config.dx);
	// k2 += (A[X + j].column(j) - A[X - j].column(j)).e(al)
	//       * (A[X + k].conj().column(k) - A[X - k].conj().column(k)).e(al)/(4.0*config.dx*config.dx);
	// k3 += (A[X + k].column(j) - A[X - k].column(j)).e(al)
	//       * (A[X + j].conj().column(k) - A[X - j].conj().column(k)).e(al)/(4.0*config.dx*config.dx);
    }

    sumgapA += gapA;
    sumA += A[X];
    
    sumkin += kin;
    sumkin_we += bfe * kin;

    sumk1 += k1;
    sumk1_we += bfe * k1;

    sumk2 += k2;
    sumk2_we += bfe *	k2;

    sumk3 += k3;
    sumk3_we += bfe *	k3;
      
    suma += a;
    suma_we += bfe * a;

    sumb1 += b1;
    sumb1_we += bfe * b1;

    sumb2 += b2;
    sumb2_we += bfe * b2;

    sumb3 += b3;
    sumb3_we += bfe * b3;

    sumb4 += b4;
    sumb4_we += bfe * b4;

    sumb5 += b5;
    sumb5_we += bfe * b5;

  } // onsites block end here

  // calculate sqrt of tr(sumA.sumA^dagger)
  sumAgap = sqrt((sumA*(sumA.dagger())).trace()); 

  std::initializer_list<int> coordsList {0,0,0};
  const CoordinateVector originpoints(coordsList);
  real_t T000 = T.get_element(originpoints);
  
  if (hila::myrank() == 0) {
        double vol = lattice.volume();
        config.stream << t << " " << T000 << " " 
	              << sumAgap.re / vol << " " << sumAgap.im / vol << " "
	              << sumgapA.re / vol << " " << sumgapA.im / vol << " "	  
	              << sumkin.re / vol << " " << sumkin.im / vol << " "
	              << sumkin_we.re / vol << " " << sumkin_we.im / vol << " "
		      << sumk1.re / vol << " " << sumk1.im / vol << " "
	              << sumk1_we.re / vol << " " << sumk1_we.im / vol << " "
		      << sumk2.re / vol	<< " " << sumk2.im / vol << " "
	              << sumk2_we.re / vol << " " << sumk2_we.im / vol << " "
		      << sumk3.re / vol	<< " " << sumk3.im / vol << " "
	              << sumk3_we.re / vol << " " << sumk3_we.im / vol << " "
		      << suma.re / vol << " " << suma.im / vol << " "
	              << suma_we.re / vol << " " << suma_we.im / vol << " "
		      << sumb1 / vol << " "
	              << sumb1_we.re / vol << " " << sumb1_we.im / vol << " "
		      << sumb2.re / vol << " " << sumb2.im / vol << " "
	              << sumb2_we.re / vol << " " << sumb2_we.im / vol << " "
		      << sumb3.re / vol << " " << sumb3.im / vol << " "
	              << sumb3_we.re / vol << " " << sumb3_we.im / vol << " "
		      << sumb4.re / vol << " " << sumb4.im / vol << " "
	              << sumb4_we.re / vol << " " << sumb4_we.im / vol << " "
		      << sumb5.re / vol << " " << sumb5.im / vol << " "
	              << sumb5_we.re / vol << " " << sumb5_we.im / vol << " "
	              << std::endl;
  }

      //hila::out0 << "Energy done \n";

} // write_energies function ends here

