#define USE_MPI 
#include <sstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
//#include <math.h>
#include <assert.h>

#include "plumbing/hila.h"
#include "plumbing/fft.h"

#include "glsol.hpp"
#include "matep.hpp"
#include "pario.hpp"

#include "ascent.hpp"
#include "conduit_blueprint.hpp"


void parIO::describeMesh_phaseExpAngle() {

    // create an vertex associated field named phaseMarker
    mesh["fields/phaseExpAngleO/association"] = "vertex";
    mesh["fields/phaseExpAngleO/topology"] = "topo";
    mesh["fields/phaseExpAngleO/values"].set_external(phaseExpAngleO.data(), latticeVolumeWithGhost);

} // describeMesh() end here

